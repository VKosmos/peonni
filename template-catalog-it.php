<?php get_header(); ?>

<main>
		<section class="catalog">
			<div class="catalog__container">

				<h1 class="catalog__title">Catalogare</h1>

				<div class="catalog__row">

					<article class="catalog__item" data-product-id="graphite" data-product-price="22500" data-product-old-price="28500" data-product-src="/assets/img/products/graphite.png" data-product-class="cart__body--graphite" data-product-name="«Геометрия»" data-product-color="Светло-графитовая">

						<div class="catalog__item-body">
							<a class="catalog__button-quick js-quick-buy" href="<?= site_url('/quick-buy'); ?>">Быстрая покупка</a>

							<a class="catalog__link" href="<?= site_url('/product-card'); ?>"></a>

							<div class="catalog__image-wrapper">
								<img class="catalog__image" src="<?= get_template_directory_uri() . "/assets/img/products/graphite.png"?>" alt="#">
							</div>

							<div class="catalog__name-row">

								<div class="catalog__name-column">
									<p class="catalog__name-color catalog__name-color--graphite">Светло-графитовая</p>
									<h2 class="catalog__name">«Геометрия»</h2>
								</div>

								<div class="catalog__color-column">
									<span class="catalog__color catalog__color--graphite"></span>
									<img class="catalog__img-color" src="<?= get_template_directory_uri() . "/assets/img/products/pattern-graphite.png"?>" alt="#">
								</div>
								
							</div>
						</div>
						</article>

						<article class="catalog__item" data-product-id="azure" data-product-price="32500" data-product-old-price="28500" data-product-src="/assets/img/products/azure.png" data-product-class="cart__body--azure" data-product-name="«Калейдоскоп»" data-product-color="Тёмно-бирюзовая">
						<div class="catalog__item-body catalog__item-body--left">
							<a class="catalog__button-quick js-quick-buy" href="<?= site_url('/quick-buy'); ?>">Быстрая покупка</a>
							<a class="catalog__link" href="<?= site_url('/product-card'); ?>"></a>

							<div class="catalog__image-wrapper">
								<img class="catalog__image" src="<?= get_template_directory_uri() . "/assets/img/products/azure.png"?>" alt="#">
							</div>

							<div class="catalog__name-row">
								<div class="catalog__name-column catalog__name-column--right">
									<p class="catalog__name-color catalog__name-color--azure">Тёмно-бирюзовый</p>
									<h2 class="catalog__name catalog__name--right">«Калейдоскоп»</h2>
								</div>
								<div class="catalog__color-column catalog__color-column--right">
									<span class="catalog__color catalog__color--azure"></span>
									<img class="catalog__img-color" src="<?= get_template_directory_uri() . "/assets/img/products/pattern-azure.png"?>" alt="#">
								</div>
							</div>
						</div>
						</article>

						<article class="catalog__item" data-product-id="dark" data-product-price="12500" data-product-old-price="28500" data-product-src="/assets/img/products/dark.png" data-product-class="cart__body--dark" data-product-name="«Калейдоскоп»" data-product-color="Черная">
						<div class="catalog__item-body">
							<a class="catalog__button-quick js-quick-buy" href="<?= site_url('/quick-buy'); ?>">Быстрая покупка</a>
							<a class="catalog__link" href="<?= site_url('/product-card'); ?>"></a>

							<div class="catalog__image-wrapper">
								<img class="catalog__image" src="<?= get_template_directory_uri() . "/assets/img/products/dark.png"?>" alt="#">
							</div>

							<div class="catalog__name-row">
								<div class="catalog__name-column">
									<p class="catalog__name-color catalog__name-color--dark">Чёрный</p>
									<h2 class="catalog__name">«Калейдоскоп»</h2>
								</div>
								<div class="catalog__color-column">
									<span class="catalog__color catalog__color--dark"></span>
									<img class="catalog__img-color" src="<?= get_template_directory_uri() . "/assets/img/products/pattern-dark.png"?>" alt="#">
								</div>
							</div>
						</div>
						</article>


				</div>
			</div>

		</section>
	</main>

<?php get_footer(); ?>
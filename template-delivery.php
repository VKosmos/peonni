<?php
/*
Template Name: Доставка и оплата
*/
?>

<?php get_header(); ?>

<main>
      <section class="delivery-payment">
         <div class="delivery-payment__container">
            <div class="delivery-payment__body">
               <div class="delivery-payment__text-section delivery-payment__text-section--delivery">
                  <h2 class="delivery-payment__title">Доставка</h2>

                  <p class="delivery-payment__text"><strong>Курьером или в пункт выдачи транспортной компании — <span
                           class="delivery-payment__row">выбирайте, что вам подходит.</span>
                     </strong></p>

                  <p class="delivery-payment__text">В любой город России мы отправляем с помощью СДЭК, <span
                        class="delivery-payment__row">ПЭК или Почтой России,
                        подбирая самые </span>оптимальные тарифы и сроки для вас.</p>

                  <p class="delivery-payment__text">C нашего российского склада срок доставки <span
                        class="delivery-payment__row">по европейской части страны составляет примерно 2-3 дня,</span> а стоимость примерно 250-600 рублей. Курьерская<span
                        class="delivery-payment__row"></span>доставка обычно на 200-300 рублей
                     дороже.</span> </p>

                  <p class="delivery-payment__text">Сообщите, какие у вас есть предпочтения по доставке <span
                        class="delivery-payment__row">или транспортной, мы все
                        учтём.</span></p>

                  <p class="delivery-payment__text"><strong>WhatsApp/Viber: +7 902 189-45-98</strong></p>

                  <p class="delivery-payment__text delivery-payment__text--margin">Предпочитаете личное общение?</p>

                  <p class="delivery-payment__text delivery-payment__text--margin"><strong>Звоните: 8 800 301-17-80</strong></p>
               </div>


               <div class="delivery-payment__text-section delivery-payment__text-section--payment">
                  <h2 class="delivery-payment__title">Оплата</h2>

                  <p class="delivery-payment__text"><strong>Возможна оплата банковской картой или наличными.<span
                           class="delivery-payment__row"> Способ
                           оплаты выбираете, какой вам
                           удобно.</span></strong></p>

                  <p class="delivery-payment__text"><span class="delivery-payment__number delivery-payment__number--one">1.</span>Можно оплатить банковской картой онлайн со скидкой 3%. Наш менеджер
                     свяжется с вами и пришлет ссылку для оплаты. Данные карты вводятся на защищенной банковской
                     платежной
                     странице в соответствии со стандартами безопасности PCI DSS, передача информации происходит с
                     применением технологии шифрования SSL по закрытым банковским сетям. После оплаты на почту вам
                     придет чек об
                     операции, подтверждающий покупку, со всеми нужными реквизитами. Чек всегда будет у вас в почте.</p>

                  

                  <p class="delivery-payment__text"><span class="delivery-payment__number delivery-payment__number--two">2.</span>Можно оплатить при получении на пункте выдачи или у <span class="delivery-payment__row">курьера.Тут возможна как наличная оплата, так и оплата</span> картой. Опять же, у вас на руках останется бумажный чек о подтверждении покупки.</p>
               </div>

               <div class="delivery-payment__text-section delivery-payment__text-section--return">

                  <h2 class="delivery-payment__title">Возврат <span
                     class="delivery-payment__row">и обмен </span></h2>

                  <p class="delivery-payment__text"><strong>Осуществляются в соответствии с Законом РФ <span
                           class="delivery-payment__row">от
                           07.02.1992 N 2300-1 (ред. от
                           24.04.2020) "О защите прав</span> потребителей"</strong></p>

                  <p class="delivery-payment__text"><img class="delivery-payment__minus delivery-payment__minus--one" src="<?= get_template_directory_uri() . "/assets/img/minus.png"?>" alt="#"><strong>Возврат товара надлежащего качества</strong></p>

                  <p class="delivery-payment__text">Вы можете передумать и вернуть товар после покупки. Такое редко, но
                     случается. Пожалуйста, убедитесь, что комплектность и коробка от коляски полностью сохранены, что
                     все
                     детали, составляющие комплект упаковки, возвращаются вами в неизменном виде. Коляска и любой из
                     элементов комплектации не могут быть со следами эксплуатации и должны быть упакованы в нашу
                     коробку.
                     Это важно! Если вы уже использовали коляску или элементы комплектации, то мы не сможем вернуть вам
                     деньги. </p>

                  <p class="delivery-payment__text">Вернуть товар можно в течение 7 календарных дней после <span
                        class="delivery-payment__row">покупки.
                        Обратная
                        доставка до продавца осуществляется за </span>счет покупателя. Денежные средства возвращаются на
                     банковскую
                     карту в течение 5-30 дней — сроки зависят от вашего банка.</p>

                  

                  <p class="delivery-payment__text"><img class="delivery-payment__minus delivery-payment__minus--two" src="<?= get_template_directory_uri() . "/assets/img/minus.png"?>" alt="#"><strong>Обмен товара надлежащего качества</strong></p>

                  <p class="delivery-payment__text">Если по какой-то причине вы решили обменять товар - например, решили, что другой цвет будет лучше, так тоже бывает - обмен коляски можно сделать в течение 14 календарных дней после покупки. В этом случае коляска и любой из элементов комплектации не могут быть в употреблении. Если коляска и все элементы комплектации будут новые, без следов эксплуатации и возвращены в нашей упаковке, мы сможем обменять коляску на другую модель.</p>

                  <p class="delivery-payment__text">В случае обмена стоимость обратной доставки товара до продавца и стоимость доставки обменной коляски оплачивается покупателем.</p>

                  <p class="delivery-payment__text"> <img class="delivery-payment__minus delivery-payment__minus--three" src="<?= get_template_directory_uri() . "/assets/img/minus.png"?>" alt="#"><strong>Возврат или обмен товара ненадлежащего качества</strong></p>

                  <p class="delivery-payment__text">Товар ненадлежащего качества — это товар, имеющий <span class="delivery-payment__row"> недостаток или существенный недостаток.</span></p>

                  <p class="delivery-payment__text">Недостаток товара — это несоответствие товара обязательным
                     требованиям,
                     предусмотренным законом, условиями договора или целям, для которых товар используется, или
                     образцу
                     и/или
                     описанию.</p>

                  <p class="delivery-payment__text">Существенный недостаток товара — это неустранимый недостаток либо недостаток, требующий несоразмерных расходов или затрат времени, либо выявленный неоднократно, либо появившийся вновь после его устранения.</p>

                  <p class="delivery-payment__text"> <strong><span>При возврате или обмене коляски, если вдруг<span class="delivery-payment__row"> оказалось, что товар был ненадлежащего качества,</span> обратная доставка до продавца и первичная доставка
                        </span> до покупателя оплачивается за счет продавца.</strong></p>

                  <p class="delivery-payment__text"><strong><span>Деньги возвращаются в течение 5-30 дней <span class="delivery-payment__row">на вашу банковскую карту. Сроки зависят от банка, </span>выпустившего карту.</span></strong></p>
               </div>
            </div>
         </div>
      </section>




<?php get_footer(); ?>
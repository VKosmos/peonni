<?php
/*
Template Name: Карточка товара
*/
?>

<?php get_header(null, ['page-card']); ?>

<main>
		<h1 class="visually-hidden">Удобные детские коляски Peonni</h1>

		<section class="card-top">

			<p class="card-top__link-wrapper">
				<a class="card-top__link" href="#">В каталог</a>
			</p>

			<div class="card-top__container">

				<div class="card-top__row">

					<div class="card-top__column card-top__column--slider">

						<div class="card-top__slider slider-product">
							<ul class="slider-product__list">
								<?php
									$id = (isset($_GET['id'])) ? $_GET['id'] : '';
								?>
								<li class="slider-product__item" data-product-id="graphite" data-product-price="22500" data-product-old-price="28500" data-product-src="/assets/img/products/graphite.png" data-product-class="cart__body--graphite" data-product-name="«Геометрия»" data-product-color="Светло-графитовая"
								<?php if ('graphite' === $id) { echo 'data-product-slide-active="0"';} ?>
								>
									<img src="<?= get_template_directory_uri() . "/assets/img/products/graphite.png"?>" alt="Peonni Светло-графитовая «Геометрия»">
								</li>
								<li class="slider-product__item" data-product-id="azure" data-product-price="32500" data-product-old-price="38500" data-product-src="/assets/img/products/azure.png" data-product-class="cart__body--azure" data-product-name="«Калейдоскоп»" data-product-color="Тёмно-бирюзовая" <?php if ('azure' === $id) { echo 'data-product-slide-active="1"';} ?>>
									<img src="<?= get_template_directory_uri() . "/assets/img/products/azure.png"?>" alt="Peonni Тёмно-бирюзовая «Калейдоскоп»">
								</li>
								<li class="slider-product__item" data-product-id="dark" data-product-price="12500" data-product-old-price="18500" data-product-src="/assets/img/products/dark.png" data-product-class="cart__body--dark" data-product-name="«Калейдоскоп»" data-product-color="Черная" <?php if ('dark' === $id) { echo 'data-product-slide-active="2"';} ?>>
									<img src="<?= get_template_directory_uri() . "/assets/img/products/dark.png"?>" alt="Peonni Черная «Калейдоскоп»">
								</li>
							</ul>
						</div>

						<div class="card-top__advantages advantages-card">
							<div class="advantages-card__wrapper">
								<ul class="advantages-card__list">
									<li class="advantages-card__item">Яркие наплечники</li>
									<li class="advantages-card__item">Авторский дизайн</li>
									<li class="advantages-card__item">Компактный размер</li>
								</ul>
								<div class="advantages-card__fade"></div>
							</div>								
						</div>

					</div>

					<div class="card-top__column card-top__column--description desc-card js-desc-card">

						<h2 class="desc-card__title">Peonni <span class="desc-card__title-new-row js-product-color">Светло-графитовая</span> <span class="desc-card__title-new-row js-product-name">«Геометрия»</span></h2>

						<div class="desc-card__flex">
							<div class="desc-card__price-wrapper">
								<p class="desc-card__price desc-card__price--old"><span class="js-product-old-price">28500</span> руб.</p>
								<p class="desc-card__price"><span class="js-product-price">22500</span> руб.</p>
							</div>
						</div>

						<div class="desc-card__text-wrapper">
							<p class="desc-card__text">Новая компактная коляска для жизни в ритме современного города. Текстильная часть сделана из водоотталкивающей натуральной ткани — прочной и стойкой к выцветанию и изнашиванию.</p>
							<p class="desc-card__text">Яркий дизайн капора и наплечников, удобный механизм складывания и раскладывания, небольшой вес, компактность и устойчивость в сложенном состоянии — основные отличительные особенности Peonni. </p>
						</div>

						<a class="desc-card__buy button button-primary js-buy" href="<?= site_url('/cart'); ?>">купить</a>

					</div>
				</div>
			</div>

		</section>

		<section class="card">

			<div class="card__container">

				<div class="card__row card__row--characteristics">

					<div class="card__column card__column--equipment">
						<h3 class="card__headline">Attrezzature</h3>
						<ul class="card__equipment-list">
							<li><a class="js-equipment-modal-open" href="#" data-src="/assets/img/modal/modal-img.jpg">Коляска</a></li>
							<li><a class="js-equipment-modal-open" href="#" data-src="/assets/img/modal/modal-img2.png">Специальный анти-москитный чехол</a></li>
							<li><a class="js-equipment-modal-open" href="#" data-src="/assets/img/modal/1920.jpg">Большой дождевик</a></li>
							<li><a class="js-equipment-modal-open" href="#" data-src="/assets/img/modal/768.jpg">Подстаканник</a></li>
							<li><a class="js-equipment-modal-open" href="#" data-src="/assets/img/modal/modal-img.jpg">Чехол для переноски</a></li>
							<li><a class="js-equipment-modal-open" href="#" data-src="/assets/img/modal/modal-img2.png">Инструкция</a></li>
						</ul>
					</div>

					<div class="card__column card__column--characteristics">
						<h3 class="card__headline card__headline--characteristics">Характеристики</h3>
						<div class="card__characteristics characteristics-card">
							<a class="characteristics-card__open-link" href="#"><span>Технические Характеристики</span></a>
							
							<ul class="characteristics-card__list">
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Вес</span>
									<span class="characteristics-card__value">6,5 кг</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Размеры в собранном виде</span>
									<span class="characteristics-card__value">25 × 49 × 56 см</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Размеры спального места</span>
									<span class="characteristics-card__value">37 × 84 см</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Высота ручки</span>
									<span class="characteristics-card__value">120 см</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Рекомендуемый возраст</span>
									<span class="characteristics-card__value">с 6 мес. до 3 лет</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Максимальный допустимый вес ребенка</span>
									<span class="characteristics-card__value">22 кг</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Диаметр колес</span>
									<span class="characteristics-card__value">13 – 15 см</span>
								</li>
								<li class="characteristics-card__item">
									<span class="characteristics-card__name">Материал рамы</span>
									<span class="characteristics-card__value">Алюминий, ткань, хлопок и полиэстер</span>
								</li>
							</ul>
						</div>
					</div>

					<button class="card__buy button button-primary">купить</button>

				</div>

				<div class="card__row card__row--others">
					<h2 class="card__headline card__headline--center">Вас может заинтересовать <span class="card__headline-new-row">Peonni в другом дизайне</span></h2>
					
					<div class="card__other-products product-card">

						<article class="product-card__item product-card__item--azure">
							<div class="product-card__body">
								<a class="product-card__link" href="<?= site_url('/product-card'); ?>"></a>
								<div class="product-card__image-wrapper">
									<img class="product-card__image" src="<?= get_template_directory_uri() . "/assets/img/products/azure.png"?>" alt="#">
								</div>
								<div class="product-card__info">

									<div class="product-card__color-wrapper">
										<span class="product-card__color"></span>
										<span class="product-card__color-img"></span>
									</div>

									<div class="product-card__text-wrapper">
										<p class="product-card__color-text">Тёмно-бирюзовый</p>
										<p class="product-card__name">«Калейдоскоп»</p>
									</div>

								</div>
							</div>
						</article>

						<article class="product-card__item product-card__item--dark product-card__item--reverse">
							<div class="product-card__body">
								<a class="product-card__link" href="<?= site_url('/product-card'); ?>"></a>
								<div class="product-card__image-wrapper">
									<img class="product-card__image" src="<?= get_template_directory_uri() . "/assets/img/products/dark.png"?>" alt="#">
								</div>
								<div class="product-card__info">

									<div class="product-card__color-wrapper">
										<span class="product-card__color"></span>
										<span class="product-card__color-img"></span>
									</div>

									<div class="product-card__text-wrapper">
										<p class="product-card__color-text">Чёрный</p>
										<p class="product-card__name">«Калейдоскоп»</p>
									</div>

								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
	</main>

	<div class="modal-info">
		<div class="modal-info__wrapper">

			<div class="modal-info__image-wrapper">
				<a class="modal-info__close" href="#"></a>
				<img class="modal-info__image" src="" alt="#">
			</div>

			<div class="modal-info__text-wrapper">
				<span class="modal-info__text">Большой дождевик</span>
			</div>

		</div>
	</div>

<?php get_footer(); ?>
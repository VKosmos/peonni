<?php get_header(); ?>

<main class="cart-page js-page-cart">
      <section class="cart">

			<button class="cart__button-close"></button>

         <div class="cart__container">
            <h1 class="cart__title"><span class="cart__none">Il tuo</span> cestino</h1>

            <div class="cart__row">

               <div class="cart__column cart__column--product">
                  <h2 class="cart__headline cart__headline--product">Заказ</h2>
                  <ul class="cart__list js-cart">

                  </ul>
                  <div class="cart__total-wrapper">
                     <p class="cart__total-name">Итого</p>
                     <p class="cart__total-price"><span class="js-cart-total-price"></span> ₽</p>
                  </div>
               </div>


               <div class="cart__column cart__column--order">
                  <h2 class="cart__headline cart__headline--order">Оформить заказ</h2>
                  <form action="" class="cart__form form-cart">
                     <input type="text" class="form-cart__input" name="name" placeholder="Имя">
                     <input type="text" class="form-cart__input" name="email" placeholder="Email">
                     <input type="text" class="form-cart__input" name="phone" placeholder="Телефон">
                     <button class="form-cart__submit-button button button-primary js-button-order-send" type="submit">купить</button>
                  </form>
               </div>

            </div>
         </div>
      </section>

   </main>



<?php get_footer(); ?>
<?php get_header(null, ['page-card']); ?>

<main>
      <section class="support">
         <div class="support__container">
            <h1 class="support__title">Garanzia e supporto</h1>
            <div class="support__column">
               <p class="support__text">Огромный накопленный опыт за время работы на рынке детских колясок позволяет нам
                  профессионально <span class="support__row">и быстро решать все проблемы сервисного характера.</span>
               </p>
               <p class="support__text">Мы рады дать расширенную гарантию на коляску Peonni - <span
                     class="support__row">2 года со дня продажи.</span></p>
               <p class="support__text">Если у вас что-то случилось, сообщите<br> на <a class="support__email" href="#">guarantee@peonni.com</a><br> или по <strong>WhatsApp/Viber <a class="support__br" href="tel:79021894598">+7 902 189 45 98.</a></strong>
               </p>
               <p class="support__text"><strong>Телефон поддержки: <a class="support__nowrap" href="tel:88003011780">8 (800) 301-17-80</a></strong></p>
               <p class="support__text">Если возможно, сразу приложите видео или фото проблемы. Мы будем на связи!</p>
               <p class="support__text">
                  Даже если у вас просто есть сложности с пониманием, 
                  <span class="support__row support__br">как работает коляска, вопросы по использованию</span> 
                  <span class="support__row support__br">каких-либо характеристик, пишите нам или звоните,</span> 
                  <span class="support__row support__br">мы подскажем и поможем быстро во всем разобраться</span>
                  <span class="support__br">в индивидуальном порядке.</span>
               </p>
            </div>
         </div>
      </section>
   </main>



<?php get_footer(); ?>
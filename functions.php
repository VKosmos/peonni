<?php
if (!defined('ABSPATH')) {
	exit;
}

show_admin_bar(false);

add_action('wp_enqueue_scripts', 'peonni_add_scripts_styles');
function peonni_add_scripts_styles()
{
	wp_enqueue_style( 'peonni-styles', get_template_directory_uri() . '/assets/css/style.css', [], null);

	wp_enqueue_script( 'peonni-slick-slider', get_template_directory_uri() . '/assets/js/slick.min.js', ['jquery'], null, true );
	wp_enqueue_script( 'peonni-scripts', get_template_directory_uri() . '/assets/js/script.js', ['jquery'], null, true );


	wp_localize_script( 'peonni-scripts', 'WPJS', [
		'siteUrl' 	=> get_template_directory_uri(),
		'ajaxUrl'	=> admin_url('admin-ajax.php'),
	] );	
}

add_action( 'template_redirect', 'redirectToLanguagePage' );
function redirectToLanguagePage() {

	$slug = get_post_field( 'post_name', get_post() );
	if (!isset($_COOKIE['language'])) {
		if ('select-language' !== $slug) {
			wp_redirect(site_url('/select-language'));
			die();
		}
	}

}

add_filter( 'template_include', 'chooseLanguageTeplateForPage');
function chooseLanguageTeplateForPage( $original_template )
{

	if (isset($_COOKIE['language'])) {
		$lang = $_COOKIE['language'];
	} else {
		return $original_template;
	}

	$slug = get_post_field( 'post_name', get_post() );
	if ('select-language' === $slug) {
		return $original_template;
	}

	switch ($lang) {
		case 'en':
			$original_template = substr($original_template, 0, strlen($original_template) -4);
			$original_template .= '-en.php';
			break;
		case 'it':
			$original_template = substr($original_template, 0, strlen($original_template) -4);
			$original_template .= '-it.php';
			break;
  	}
	return $original_template;
}


# 
# MAIL
# 
add_action('wp_ajax_send_email_quick', 'peonni_send_email_quick');
add_action('wp_ajax_nopriv_send_email_quick', 'peonni_send_email_quick');
function peonni_send_email_quick()
{
	$method = $_SERVER['REQUEST_METHOD'];
	if ($method !== 'POST') {
	  	exit();
	}

	$temp = sanitize_text_field($_POST['client']);
	$client_arr = explode('&&&', $temp); // 0 Имя, 1 телефон, 2 email
	
	$temp = sanitize_text_field($_POST['product']);
	$product = explode('&&&', $temp); // id, name, price

	$message = "
		<tr style='background-color: #f8f8f8;'>
			<td style='padding: 10px; border: 1px solid #e9e9e9;'>$product[0]</td>
			<td style='padding: 10px; border: 1px solid #e9e9e9;'>$product[1]</td>
			<td style='padding: 10px; border: 1px solid #e9e9e9;'>$product[2]</td>
		</tr>";

	$message = "<table style='width: 100%;'><tr><td>ID</td><td>Наименование</td><td>Цена</td></tr>$message</table>";	

	$message = '<h3>' . $client_arr[0] . ', ' . $client_arr[1] . ', ' . $client_arr[2] . '</h3>' . "\r\n" . $message;

	$admin_email = 'pizza-orders@fls-master.ru';
	$form_subject = 'Быстрая покупка Peonni';

	$headers  = "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= "From: $form_subject <$admin_email>\r\n";

	$success_send = wp_mail( $admin_email, $form_subject, $message, $headers );
	
	if ($success_send) {
		echo 'success';
	} else {
		echo 'error';
	}
	
	wp_die();	
}


add_action('wp_ajax_send_email', 'peonni_send_email');
add_action('wp_ajax_nopriv_send_email', 'peonni_send_email');

function peonni_send_email() 
{
	$method = $_SERVER['REQUEST_METHOD'];
	if ($method !== 'POST') {
	  	exit();
	}

	$temp = sanitize_text_field($_POST['client']);
	$client_arr = explode('&&&', $temp); // 0 Имя, 1 телефон, 2 email
	
	$temp = sanitize_text_field($_POST['products']);
	$products_arr = explode('&&&', $temp); // []: id, name, quantity, price


	foreach ($products_arr as $item) {
		$v = explode(',', $item);

		$color = $color_counter % 2 === 0 ? '#fff' : '#f8f8f8';
		$str = "
		  	<tr style='background-color: $color;'>
				<td style='padding: 10px; border: 1px solid #e9e9e9;'>$v[0]</td>
				<td style='padding: 10px; border: 1px solid #e9e9e9;'>$v[1]</td>
				<td style='padding: 10px; border: 1px solid #e9e9e9;'>$v[2]</td>
				<td style='padding: 10px; border: 1px solid #e9e9e9;'>$v[3]</td>
			</tr>";
	
		$message .= $str;
		$color_counter++;
	}

	$message = "<table style='width: 100%;'><tr><td>ID</td><td>Наименование</td><td>Количество</td><td>Цена</td></tr>$message</table>";	

	$message = '<h3>' . $client_arr[0] . ', ' . $client_arr[1] . ', ' . $client_arr[2] . '</h3>' . "\r\n" . $message;

	$admin_email = 'pizza-orders@fls-master.ru';
	$form_subject = 'Заказ Peonni';

	$headers  = "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= "From: $form_subject <$admin_email>\r\n";

	$success_send = wp_mail( $admin_email, $form_subject, $message, $headers );
	
	if ($success_send) {
		echo 'success';
	} else {
		echo 'error';
	}
	
	wp_die();
	
}
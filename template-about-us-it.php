<?php get_header(); ?>

<main>
	<section class="about-us">
		<div class="about-us__container">

			<h1 class="about-us__title">Riguardo a noi</h1>

			<div class="about-us__body">

				<div class="about-us__text-wrapper">
					<p class="about-us__text about-us__text--bold">Дизайнерское вдохновение и многолетняя история работы на рынке прогулочных колясок дали рождение детским коляскам Peonni.</p>

					<p class="about-us__text about-us__text--bold">
						Мы изучали этот рынок и наблюдали, что предпочтение отдается легким коляскам с удобным механизмом
						складывания, но на наш взгляд им очень не хватало качества и внешней индивидуальности. Так появился
						бренд Peonni.
					</p>
				</div>

				<div class="about-us__content-wrapper">
					<div class="about-us__image-wrapper about-us__image-wrapper--one">
						<img class="about-us__image" src="<?= get_template_directory_uri() . "/assets/img/about_us/about-1.jpg"?>" alt="#">
					</div>

					<div class="about-us__headline-wrapper about-us__headline-wrapper--one">
						<p class="about-us__headline">
							Мы настолько уверены в высоком качестве нашей коляски,
							<span class="about-us__headline-new-row about-us__headline-left">что смело даем </span>
							<span class="about-us__headline-new-row about-us__headline-left">гарантию 2 года </span>
							<span class="about-us__headline-new-row">со дня продажи.</span>
						</p>
					</div>
				</div>

				<div class="about-us__text-wrapper">
					<p class="about-us__text">
						В создании Peonni мы вдохновлялись молодыми активными мамами, такими яркими, модными и разными.
						Которые успевают главное — наслаждаться жизнью вместе со своим малышом. Мы знаем, что в этот период
						жизни коляска является главным помощником и сопровождает маму везде — в поездках, в походах в
						магазин и торговый центр, в парк и в кафе и еще много куда. Поэтому важно, чтобы она была удобной,
						легкой, всегда красивой и приносящей радость при ее использовании.
					</p>
					<p class="about-us__text">
						Бренд Peonni появился в 2018 году. Тогда мы взяли в команду креативного директора, вместе с которым
						с тех пор создали и продолжаем создавать уникальные авторские паттерны для текстильной части
						коляски. Яркие наплечники и верх капа привлекают внимание, радуют глаз и эффектно выглядят на
						семейных фото.
					</p>
				</div>

				<div class="about-us__content-wrapper">

					<div class="about-us__image-wrapper about-us__image-wrapper--two">
						<img class="about-us__image" src="<?= get_template_directory_uri() . "/assets/img/about_us/about-2.jpg"?>" alt="#">
					</div>

					<div class="about-us__text-wrapper">

						<p class="about-us__text">По поводу ткани для капа было много обсуждений, сомнений и многомесячного
							тестирования свойств. Мы мяли, стирали, лили воду из лейки, терли, пачкали и что только не
							делали — и в результате остановились на европейском качестве и натуральности — нам было очень
							важно, чтобы текстиль отвечал высоким стандартам и содержал хлопок, так как этой части
							непосредственно касается ребенок. Также, было главное требование сохранить как можно дольше
							внешний вид коляски. Поэтому ткань выбрали с плотным саржевым плетением и с характеристиками
							повышенной прочности, водостойкости и стойкости к выцветанию. Поэтому Peonni хватит и на
							первого, и на второго, и на третьего малыша — а может, и дольше! </p>
					</div>
				</div>

				<div class="about-us__content-wrapper">
					<div class="about-us__image-wrapper about-us__image-wrapper--three">
						<img class="about-us__image" src="<?= get_template_directory_uri() . "/assets/img/about_us/about-3.jpg"?>" alt="#">
					</div>

					<div class="about-us__headline-wrapper about-us__headline-wrapper--two">
						<p class="about-us__headline">
							<span class="about-us__headline-left">Яркие наплечники и верх капа</span>
							<span class="about-us__headline-new-row about-us__headline-left">привлекают внимание, </span>
							радуют глаз
							<span class="about-us__headline-new-row">и эффектно выглядят на семейных фото.</span>
						</p>
					</div>
				</div>

				<div class="about-us__text-wrapper">
					<p class="about-us__text">В ходе разработки коляски конструкторы внесли ряд улучшений в текстильный
						блок и раму, добавив необходимые детали для удобства мамы и малыша. Так, мы сделали раму прочнее,
						увеличили капюшон коляски, увеличили подножку для удлинения спального места, соединили корзину и
						спинку коляски сеткой, улучшили окошко присмотра и добавили еще несколько небольших, но
						качественных изменений.</p>
					<p class="about-us__text">В 2020 году после всех экспериментальных образцов коляска Peonni стала
						доступна для покупателей.</p>
				</div>

				<div class="about-us__content-wrapper">

					<div class="about-us__image-wrapper about-us__image-wrapper--four">
						<img class="about-us__image" src="<?= get_template_directory_uri() . "/assets/img/about_us/about-4.jpg"?>" alt="#">
					</div>

					<div class="about-us__text-wrapper">
						<p class="about-us__text">Мы надеемся, что коляска Peonni станет частью вашей повседневной жизни и
							всегда готовы услышать ваше мнение о качестве, любые предложения по улучшению и просто отзывы о
							нашем продукте.</p>
						<p class="about-us__text">А мы настолько уверены в высоком качестве нашей коляски, что смело даем
							гарантию 2 года со дня продажи. 💗</p>
					</div>
				</div>

				<a class="about-us__button button button-testiary" href="<?= site_url('/catalog'); ?>">выбрать цвет и купить</a>

			</div>

		</div>
	</section>
</main>



<?php get_footer(); ?>
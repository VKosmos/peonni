<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Peonni</title>

	<?php wp_head(); ?>
</head>

<body <?= isset($args[0]) ? "class='" . $args[0] . "'" : '' ?>>

	<header class="header">
		<div class="header__container">
  
			<div class="header__logo-wrapper">
				<a href="<?= site_url() ?>">
					<img class="header__logo-image" src="<?= get_template_directory_uri() . "/assets/img/header/logo.png"?>" alt="#">
				</a>
			</div>

			<div class="header__lang-wrapper">
				<a class="js-select-language" href="<?= site_url(); ?>" data-lang="it">
					<img class="header__lang" src="<?= get_template_directory_uri() . "/assets/img/flags/lang-it.svg"?>" alt="Italiano" width="18" height="12">
				</a>
				<a class="js-select-language" href="<?= site_url(); ?>" data-lang="ru">
					<img class="header__lang" src="<?= get_template_directory_uri() . "/assets/img/flags/lang-ru.svg"?>" alt="Русский" width="18" height="12">
				</a>
				<a class="js-select-language" href="<?= site_url(); ?>" data-lang="en">
					<img class="header__lang" src="<?= get_template_directory_uri() . "/assets/img/flags/lang-uk.svg"?>" alt="English" width="18" height="12">
				</a>
			</div>

			<p class="header__buy"><a class="header__buy-link" href="<?= site_url('/catalog'); ?>">Купить</a></p>

			<button class="header__btn-menu-open" type="button"></button>
		</div>
	</header>
<?php get_header(); ?>

<main>
      <section class="contacts">
         <div class="contacts__container">

            <div class="contacts__wrapper">
               <h1 class="contacts__title">Contatti</h1>
               <div class="contacts__address-wrapper">
                  <p class="contacts__country">Российская Федерация</p>
                  <p class="contacts__text">г. Самара, Самарская область, <br> ул. Московское шоссе, д.22 стр.2.</p>
                  <p class="contacts__text"><a class="contacts__link" href="tel:78003011780">+7 800 301-17-80</a></p>
                  <p class="contacts__text"><a class="contacts__link" href="mailto:info@yoya-plus.com">info@yoya-plus.com</a></p>               </div>
            </div>

            <div class="contacts__form contacts-form">
               <h2 class="contacts-form__title">Напишите нам</h2>
               <form class="contacts-form__form">
                  <input class="contacts-form__input" type="text" placeholder="Имя">
                  <input class="contacts-form__input" type="text" placeholder="Адрес email">
                  <textarea class="contacts-form__text" placeholder="Текст сообщения"></textarea>
                  <button class="contacts-form__button" type="submit">отправить</button>
               </form>
            </div>
         </div>
      </section>
   </main>


<?php get_footer(); ?>
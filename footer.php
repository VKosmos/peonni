<footer class="footer">

<div class="footer__container">

	<div class="footer__row">

		<nav class="footer__nav">
			<h2 class="visually-hidden">Меню сайта</h2>
			<ul class="footer__menu-list">
				<li class="footer__menu-item">
					<a href="<?= site_url('/catalog'); ?>">Каталог</a>
				</li>
				<li class="footer__menu-item">
					<a href="<?= site_url('/delivery'); ?>">Оплата и доставка</a>
				</li>
				<li class="footer__menu-item">
					<a href="<?= site_url('/about-us'); ?>">О Peonni</a>
				</li>
				<li class="footer__menu-item">
					<a href="tel:88007754063">8 800 775-40-63</a>
				</li>
			</ul>
		</nav>

		<ul class="footer__social social-list">
			<li class="social-list__item">
				<a href="#">
					<img class="social-list__image social-list__image--vk" src="<?= get_template_directory_uri() . "/assets/img/footer/vk.svg"?>" alt="#">
				</a>
			</li>
			<li class="social-list__item">
				<a href="#">
					<img class="social-list__image social-list__image--instagram" src="<?= get_template_directory_uri() . "/assets/img/footer/instagram.svg"?>"
						alt="#">
				</a>
			</li>
			<li class="social-list__item">
				<a href="#">
					<img class="social-list__image social-list__image--youtube" src="<?= get_template_directory_uri() . "/assets/img/footer/youtube.svg"?>" alt="#">
				</a>
			</li>
		</ul>
	</div>

	<div class="footer__row">
		<a class="footer__logo-link" href="<?= site_url(); ?>">
			<img class="footer__logo" src="<?= get_template_directory_uri() . "/assets/img/footer/footer-logo.svg"?>" alt="#">
		</a>
		<p class="footer__copyright">© 2020. Торговая марка зарегистрирована в РФ </p>
	</div>

</div>
</footer>



<div class="modal-menu">
	<div class="modal-menu__header">
		<div class="modal-menu__row">
			<div class="modal-menu__logo-wrapper">
				<a href="index.html">
					<img class="modal-menu__logo-image" src="<?=get_template_directory_uri() . "/assets/img/header/logo.png"?>" alt="#">
				</a>
			</div>

			<p class="modal-menu__buy"><a class="modal-menu__buy-link" href="<?= site_url('/catalog'); ?>">Купить</a></p>
			<button class="modal-menu__btn-menu-close" type="button"></button>
		</div>

		<div class="modal-menu__lang-wrapper">
			<a class="js-select-language" href="<?= site_url(); ?>" data-lang="it">
				<img class="modal-menu__lang" src="<?=get_template_directory_uri() . "/assets/img/flags/lang-it.svg"?>" alt="#" width="30" height="20">
			</a>
			<a class="js-select-language" href="<?= site_url(); ?>" data-lang="ru">
				<img class="modal-menu__lang" src="<?=get_template_directory_uri() . "/assets/img/flags/lang-ru.svg"?>" alt="#" width="30" height="30">
			</a>
			<a class="js-select-language" href="<?= site_url(); ?>" data-lang="en">
				<img class="modal-menu__lang" src="<?=get_template_directory_uri() . "/assets/img/flags/lang-uk.svg"?>" alt="#" width="30" height="20">
			</a>
		</div>

	</div>


	<div class="modal-menu__wrapper">

		<div class="modal-menu__body">
			<nav class="modal-menu__nav">
				<h2 class="visually-hidden">Главное меню сайта</h2>
				<ul class="modal-menu__menu-list">
					<li class="modal-menu__menu-item"><a href="<?= site_url(); ?>">Главная</a></li>
					<li class="modal-menu__menu-item"><a href="<?= site_url('/catalog'); ?>">Каталог</a></li>
					<li class="modal-menu__menu-item"><a href="<?= site_url('/gallery'); ?>">Галерея</a></li>
					<li class="modal-menu__menu-item"><a href="<?= site_url('/delivery'); ?>>Оплата и доставка</a></li>
					<li class="modal-menu__menu-item"><a href="<?= site_url('/about-us'); ?>">О Peonni</a></li>
					<li class="modal-menu__menu-item"><a href="<?= site_url('/support'); ?>">Поддержка</a></li>
					<li class="modal-menu__menu-item"><a href="<?= site_url('/contacts'); ?>">Контакты</a></li>
				</ul>
			</nav>

			<ul class="modal-menu__social social-list">
				<li class="social-list__item">
					<a href="#">
						<img class="social-list__image social-list__image--vk" src="<?= get_template_directory_uri() . '/assets/img/header/vk.svg'?>" alt="#">
					</a>
				</li>
				<li class="social-list__item">
					<a href="#">
						<img class="social-list__image social-list__image--instagram" src="<?= get_template_directory_uri() . '/assets/img/header/instagram.svg'?>"
							alt="#">
					</a>
				</li>
				<li class="social-list__item">
					<a href="#">
						<img class="social-list__image social-list__image--youtube" src="<?= get_template_directory_uri() . '/assets/img/header/youtube.svg'?>" alt="#">
					</a>
				</li>
			</ul>
		</div>

	</div>
</div>


	<?php wp_footer(); ?>

</body>

</html>
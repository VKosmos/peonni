<?php get_header(); ?>

	<main>
		<h1 class="visually-hidden">Comfortable child carriage Peonni</h1>
		<section class="top-banner">

			<div class="top-banner__wrapper">
				
				<p class="top-banner__text">Comfortable<br><span class="top-banner__left-indent-big">child</span> <br><span class="top-banner__left-indent-small">carriage</span></p>
				
				<a class="top-banner__button button button-primary" href="catalog.html">Добавить в корзину</a>
				
				<a class="top-banner__arrow-link" href="#scroll-to"></a>
			</div>

		</section>

		<section class="info">

			<div class="info__container">
				<div class="info__row" id="scroll-to">

					<div class="info__column info__column--left">
						<div class="info__text-wrapper">

							<h2 class="info__title">Peonni - <span class="info__underline">это лучшая</span> <span class="info__title-new-row">компактная коляска,</span></h2>

							<p class="info__text">которая <span class="info__text-new-row">складывается</span> <span class="info__text-new-row">и раскладывается</span> <span class="info__text-new-row">в одно движение</span></p>

						</div>
					</div>
					<div class="info__column info__column--right">
						<div class="info__image-wrapper info__image-wrapper--right">
							<img class="info__image info__image--one" src="<?= get_template_directory_uri() . "/assets/img/main/img-1.jpg"?>" alt="#">
						</div>  
					</div>

				</div>

				<div class="info__row">

					<div class="info__column info__column--left info__column--min">

						<div class="info__image-wrapper">
							<img class="info__image info__image--two" src="<?= get_template_directory_uri() . "/assets//img/main/img-2.jpg"?>" alt="#">
						</div>  

					</div>

					<div class="info__column info__column--right info__column--big">

						<div class="info__text-wrapper info__text-wrapper--two">

							<h2 class="info__title info__title--gold">С Peonni легко выглядеть</h2>

							<h3 class="info__headline">Модно <span>&</span> Стильно</h3>
							
							<p class="info__text-right">Для прогулок по городу, далеких и близких путешествий, походов по магазинам и встреч в кафе</p>

						</div>
								
					</div>

				</div>

			</div>
		</section>

		<section class="info2">
			<div class="info2__container">
				
				<div class="info2__row">

					<div class="info2__column">

						<div class="info2__item info2__item--small">
							<div class="info2__wrapper1">
								<p class="info2__title">Правильно подобранная коляска <span>дополняет образ молодой мамы</span></p>
							</div>
						</div>

						<div class="info2__item">
							<div class="info2__wrapper2">
								<div class="info2__image-wrapper2">
									<img src="<?= get_template_directory_uri() . "/assets/img/main/img-4.jpg"?>" alt="">
								</div>
								<div class="info2__label-wrapper2">
									<span class="info2__color"></span>
									<p class="info2__text">Гуляйте по улицам города с комфортом</p>
								</div>
							</div>
						</div>

						<div class="info2__item da-target3">
							<div class="info2__wrapper3" data-da="da-target4, 1, 767">
								<div class="info2__image-wrapper3">
									<img src="<?= get_template_directory_uri() . "/assets/img/main/img-6.jpg"?>" alt="">
								</div>
								<div class="info2__label-wrapper3">
									<span class="info2__color"></span>
									<p class="info2__text">Проводите время на свежем воздухе, не опасаясь УФ-лучей</p>
								</div>
							</div>
						</div>

					</div>
					
					<div class="info2__column">

						<div class="info2__item da-target4">
							<div class="info2__wrapper4" data-da="da-target3, 1, 767">
								<div class="info2__image-wrapper4">
									<img src="<?= get_template_directory_uri() . "/assets/img/main/img-3.jpg"?>" alt="">
								</div>
								<div class="info2__label-wrapper4">
									<span class="info2__color"></span>
									<p class="info2__text">Путешествуйте <span class="info2__text-new-row">без лишних усилий</span></p>
								</div>
							</div>
						</div>		
						
						<div class="info2__item">
							<div class="info2__wrapper5">
								<div class="info2__image-wrapper5">
									<img src="<?= get_template_directory_uri() . "/assets/img/main/img-5.jpg"?>" alt="">
								</div>
								<div class="info2__label-wrapper5">
									<span class="info2__color"></span>
									<p class="info2__text">Оцените легкость <span class="info2__text-new-row">и компактность Peonni</span></p>
								</div>
							</div>
						</div>		

						<div class="info2__item">
							<div class="info2__wrapper6">
								<p class="info2__title">Peonni помогает вести активную жизнь <span>и не превратиться в домоседку</span></p>
							</div>
						</div>		
						
					</div>


				</div>



			</div>
		</section>

		<section class="choose">
			<div class="choose__container">

				<div class="choose__title-wrapper">
					<h2 class="choose__title">Выберите один <span class="choose__text-new-row">из вариантов</span> <span class="choose__text-new-row">авторского дизайна</span></h2>
					<a href="<?= site_url('/catalog'); ?>" class="choose__button button button-primary">выбрать</a>
				</div>

				<div class="choose__flex">

					<article class="choose__item choose__item--azure">
						<div class="choose__body">
							<div class="choose__image-wrapper">
								<img class="choose__image" src="<?= get_template_directory_uri() . "/assets/img/products/azure.png"?>" alt="#">
							</div>
							<div class="choose__color-wrapper">
								<span class="choose__color"></span>
								<span class="choose__color-img"></span>
							</div>
						</div>
					</article>

					<article class="choose__item choose__item--graphite choose__item--down-high">
						<div class="choose__body">
							<div class="choose__image-wrapper">
								<img class="choose__image" src="<?= get_template_directory_uri() . "/assets/img/products/graphite.png"?>" alt="#">
							</div>
							<div class="choose__color-wrapper">
								<span class="choose__color"></span>
								<span class="choose__color-img"></span>
							</div>
						</div>
					</article>

					<article class="choose__item choose__item--dark choose__item--down-low ">
						<div class="choose__body">
							<div class="choose__image-wrapper">
								<img class="choose__image" src="<?= get_template_directory_uri() . "/assets/img/products/dark.png"?>" alt="#">
							</div>
							<div class="choose__color-wrapper">
								<span class="choose__color"></span>
								<span class="choose__color-img"></span>
							</div>
						</div>
					</article>

				</div>

			</div>

		</section>		

		<section class="dignity">
			<div class="dignity__container">
				<div class="dignity__title-wrapper">
					<h2 class="dignity__title">Оцените <span class="dignity__title-new-row">Все достоинства коляски</span></h2>
				</div>

				<div class="dignity__flex">

					<div class="dignity__item dignity__item--one">
						<div class="dignity__body">
							<span class="dignity__index dignity__index--odd">1</span>
							<div class="dignity__image-wrapper">
								<img class="dignity__image" src="<?= get_template_directory_uri() . "/assets/img/main/img-7.png"?>" alt="#">
							</div>

							<div class="dignity__text-wrapper">
								<p class="dignity__description">Лёгкая <span class="dignity__text-new-row">и суперкомпактная</span></p>
								<p class="dignity__text">Помещается в любой багажник, занимает минимум места дома, в сложенном состоянии стоит без опоры</p>
							</div>
						</div>
					</div>

					<div class="dignity__item dignity__item--two">
						<div class="dignity__body">
							<span class="dignity__index dignity__index--even">2</span>
							<div class="dignity__image-wrapper">
								<img class="dignity__image" src="<?= get_template_directory_uri() . "/assets/img/main/img-8.jpg"?>" alt="#">
							</div>
							<div class="dignity__text-wrapper">
								<p class="dignity__description">Европейская натуральная ткань</p>
								<p class="dignity__text">Материал с гипоаллергенными свойствами и сверхпрочным плетением.  Водоотталкивающая ткань защищает в непогоду </p>
							</div>
						</div>
					</div>

				</div>

				<div class="dignity__flex">

					<div class="dignity__item dignity__item--three">
						<div class="dignity__body">
							<span class="dignity__index dignity__index--odd">3</span>
							<div class="dignity__image-wrapper">
								<img class="dignity__image" src="<?= get_template_directory_uri() . "/assets/img/main/img-1.jpg"?>" alt="#">
							</div>

							<div class="dignity__text-wrapper">
								<p class="dignity__description">Складывается и раскладывается в одно движение</p>
							</div>
						</div>
					</div>

					<div class="dignity__item dignity__item--four">
						<div class="dignity__body">
							<span class="dignity__index dignity__index--even">4</span>
							<div class="dignity__image-wrapper">
								<img class="dignity__image" src="<?= get_template_directory_uri() . "/assets/img/main/img-9.png"?>" alt="#">
							</div>
							<div class="dignity__text-wrapper">
								<p class="dignity__description">Подходит <span class="dignity__text-new-row">для малышей 0+</span></p>
								<p class="dignity__text">Спинка откидывается до горизонтального состояния. Удлиненный кап защищает от солнца, ветра и непогоды. Москитная сетка надежно крепится на молнию</p>
							</div>
						</div>
					</div>

				</div>

			</div>
		</section>

		<section class="citation">
			<div class="citation__container">
				<div class="citation__body">
					<span class="citation__quotes">”</span>

					<div class="citation__wrapper">
						<img class="citation__author-image" src="<?= get_template_directory_uri() . "/assets/img/main/citation-author.png"?>" alt="#">

						<p class="citation__content">

							<span class="citation__headline">
								Нам было важно сделать коляску
								<span class="citation__headline-new-row"> с модным дизайном, <span class="span citation__headline-left-row">сохранив </span></span>
								<span class="citation__headline-new-row"> 
									максимальный 
									<span class="span citation__headline-new-row-md2">комфорт малыша.</span>
								</span>
								</span>
							</span>		

							<span class="citation__text"><span class="citation__text-new-row">Благодаря натуральным материалам высокого качества</span> <span class="citation__text-new-row">коляска идеальна для современных стильных мам, ведущих</span> активный образ жизни.</span>

						</p>
						
					</div>
					<p class="citation__signature">Александра Мотова <span>Креативный директор Peonni</span></p>
				</div>
			</div>
		</section>		

		<section class="video-review">
			<div class="video-review__container">

				<div class="video-review__wrapper">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/2OEL4P1Rz04" frameborder="0"
						allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>

				<div class="video-review__text-wrapper">
					<h2 class="video-review__title">Видеообзор</h2>
					<p class="video-review__text">Наглядно о суперсвойствах Peonni</p>
				</div>

			</div>
		</section>

		<section class="bottom-banner">
			<div class="bottom-banner__container">

				<div class="bottom-banner__row">

					<div class="bottom-banner__column">
						<div class="bottom-banner__image-wrapper">
							<img class="bottom-banner__image" src="<?= get_template_directory_uri() . "/assets/img/main/img-010.jpg"?>" alt="#">
						</div>
					</div>

					<div class="bottom-banner__column">
						<div class="bottom-banner__text-wrapper">
							<h2 class="bottom-banner__title">Детская коляска <span>peonni</span></h2>
							<p class="bottom-banner__text">
								Создана с заботой 
								<span class="bottom-banner__text-new-row">о комфорте 
									<span class="bottom-banner__text-new-row-md2">ребенка для удобства </span>
								</span>
								<span class="bottom-banner__text-new-row">в повседневной жизни.</span>
							</p>
							<a class="bottom-banner__button button button-testiary" href="<?= site_url('/catalog'); ?>">Выбрать цвет и купить</a>
						</div>
					</div>

				</div>

			</div>
		</section>

		<section class="mailing">
			<div class="mailing__container">
				<h2 class="mailing__title">Рассылка</h2>
				<div class="mailing__row">
					<div class="mailing__column">
						<p class="mailing__text">Оставьте ваш email, чтобы <strong>получать полезную информацию о наших новинках и специальных предложениях.</strong></p>
					</div>
					<div class="mailing__column">
						<form class="mailing__form" action="#" method="post">
							<input class="mailing__input" type="text" placeholder="Адрес email">
							<button class="mailing__submit-button" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</section>


	</main>

	<?php get_footer(); ?>
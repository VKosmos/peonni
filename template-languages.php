<?php
/*
Template Name: Выбор языка
*/
?>

<?php get_header('languages'); ?>


	<main>
		<section class="lang">
			<div class="lang__container">
				<h1 class="visually-hidden">Выберите язык сайта</h1>

				<div class="lang__body">
					
					<div class="lang__logo-wrapper">
						<img src="<?= get_template_directory_uri() . "/assets/img/logo.svg"?>" alt="" class="lang__logo">
						<img src="<?= get_template_directory_uri() . "/assets/img/registered-logo.svg"?>" alt="" class="lang__registered-logo">
					</div>

					<div class="lang__image-wrapper">
						<img src="<?= get_template_directory_uri() . "/assets/img/products/azure.png"?>" alt="" class="lang__image">
					</div>
				
					<div class="lang__buttons buttons-lang">
						<p class="buttons-lang__headline">Comfortable strollers</p>
						<ul class="buttons-lang__list">
							<li class="buttons-lang__item">
								<a class="js-select-language" href="<?= site_url(); ?>" data-lang="it"></a>
								<img src="<?= get_template_directory_uri() . "/assets/img/flags/lang-it.svg"?>" alt="">
								<p>Italiano</p>
							</li>
							<li class="buttons-lang__item">
								<a class="js-select-language" href="<?= site_url(); ?>" data-lang="ru"></a>
								<img src="<?= get_template_directory_uri() . "/assets/img/flags/lang-ru.svg"?>" alt="">
								<p>Русский</p>
							</li>
							<li class="buttons-lang__item">
								<a class="js-select-language" href="<?= site_url(); ?>" data-lang="en"></a>
								<img src="<?= get_template_directory_uri() . "/assets/img/flags/lang-uk.svg"?>" alt="">
								<p>English</p>
							</li>
						</ul>
					</div>

					<p class="lang__copyright">© All rights reserved <span>2021</span></p>

				</div>

			</div>
		</section>
	</main>


	<?= wp_footer(); ?>

</body>

</html>
jQuery(function ($) {
	
	(function () { 

		/*
		 *	Основное меню и модалки
		 */
		let modal_menu = $('.modal-menu');
		let html = $('html');
		let modal_info = $(".modal-info");
	
		$('.header__btn-menu-open').on('click', (ev) => {
			ev.preventDefault();
			modal_menu.addClass('js-shown');
			html.addClass('lock');
		})
	
		$('.modal-menu__btn-menu-close').on('click', (ev) => {
			ev.preventDefault();
			modal_menu.removeClass('js-shown');
			html.removeClass('lock');
		})
	
		$('.modal-menu').on('click', function (ev) {
			if (this === ev.target) {
				modal_menu.removeClass('js-shown');
				html.removeClass('lock');
			}
		})

		$('.js-equipment-modal-open').on('click', function (ev) {
			ev.preventDefault();
			let src = $(this).attr('data-src');
			src = WPJS.siteUrl + src;
			modal_info.find('.modal-info__image').attr('src', src);
			html.addClass('lock');
			modal_info.addClass('js-shown');
		});
	
		$('.modal-info__close').on('click', (ev) => {
			ev.preventDefault();
			modal_info.removeClass('js-shown');
			html.removeClass('lock');
		})
	
		modal_info.on('click', function (ev) {
			if (this === ev.target) {
				modal_info.removeClass('js-shown');
				html.removeClass('lock');
			}
		})		
	
		$(document).on('keydown', (ev) => {
			if (ev.keyCode === 27) {
				if (modal_menu.hasClass('js-shown')) {
					html.removeClass('lock');
					modal_menu.removeClass('js-shown');
				}

				if (modal_info) {
					if (modal_info.hasClass('js-shown')) {
						html.removeClass('lock');
						modal_info.removeClass('js-shown');
					}
				}
			};
		});
	
	
		/*
		*	Анимация скролла
		*/
		$('.top-banner__arrow-link').on('click', function (ev) {
			let href = $(this).attr('href');
	
			$('html, body').animate({
				scrollTop: $(href).offset().top
			}, {
					duration: 500,
				easing: "linear"
			});
	
			return false;
		});	
	
		/*
		*	Слайдер в карточке товаров
		*/

		let active_slide = ($('.slider-product__item[data-product-slide-active]').length) ? parseInt($('.slider-product__item[data-product-slide-active]').attr('data-product-slide-active')) : 0;

		$('.slider-product__list').on('init', function () {
			let activeSlideE = $('.slider-product__item.slick-current');
			targetE = $('.js-desc-card');
			if (targetE) {
				targetE.find('.js-product-name').text(activeSlideE.attr('data-product-name'));
				targetE.find('.js-product-color').text(activeSlideE.attr('data-product-color'));
				targetE.find('.js-product-price').text(activeSlideE.attr('data-product-price'));
				targetE.find('.js-product-old-price').text(activeSlideE.attr('data-product-old-price'));
			}
		})

		$('.slider-product__list').slick({
			arrows: true,
			dots: true,
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			appendDots: '.desc-card__flex',
			dotsClass: 'desc-card__products-buttons-list',
			asNavFor: '.advantages-card__list',
			initialSlide: active_slide,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						arrows: false,
					}
				},
			],
		});
	
		$('.slider-product__list').on('afterChange', (currentSlide) => {
			currentProduct = $(currentSlide.currentTarget).find('.slick-current');
			targetE = $('.js-desc-card');
	
			if (!targetE) {
				return;
			}
			targetE.find('.js-product-name').text(currentProduct.attr('data-product-name'));
			targetE.find('.js-product-color').text(currentProduct.attr('data-product-color'));
			targetE.find('.js-product-price').text(currentProduct.attr('data-product-price'));
			targetE.find('.js-product-old-price').text(currentProduct.attr('data-product-old-price'));
			
		})
	
		$('.slider-product__list').on('breakpoint', function () {
			initSlickButtons();
		});
	
		$('.advantages-card__list').slick({
			arrows: false,
			dots: false,
			infinite: false,		
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			centerMode: true,
			variableWidth: true,
			asNavFor: '.slider-product__list',
			initialSlide: active_slide,
		});
	
	
		function initSlickButtons() {
			let buttons = $('.desc-card__products-buttons-list button');
	
			buttons.each(function () {
				
				let slideId = $(this).attr('aria-controls');
				let product = $('#' + slideId);
		
				if (product) {
					color = product.attr('data-product-id');
					switch (color) {
						case 'azure':
							$(this).parent().addClass('azure');
							break;
						case 'graphite':
							$(this).parent().addClass('graphite');
							break;
						case 'dark':
							$(this).parent().addClass('dark');
							break;
					}
				}
			});
		}
		initSlickButtons();
		
	
		/*
		 *	Открытие характеристик в карточке товара
		 */
		$('.characteristics-card__open-link').on('click', function(ev) {
			ev.preventDefault();
			button = $(this);
			
			if (button.hasClass('js-active')) {
				$(this).removeClass('js-active');
				$(this).siblings('.characteristics-card__list').slideUp();
			} else {
				$(this).addClass('js-active');
				$(this).siblings('.characteristics-card__list').slideDown();
			}
	
		})


		/*
		 *	Корзина
		 */
		
		let cart = JSON.parse(localStorage.getItem('cart')) || {};
		let quick = JSON.parse(localStorage.getItem('quick')) || {};
		let cartE = $('.js-cart');
		let quickE = $('.js-quick')
		let cartTotalPriceE = $('.js-cart-total-price');
		let quickTotalPriceE = $('.js-quick-total-price');
	
		// На странице Карточки товара - получение всех данных товара
		const getProductData = (el) => {
	
			let id = el.attr('data-product-id');
	
			if (!id) { return }
	
			let name = el.attr('data-product-name');
			let price = el.attr('data-product-price');
			let src = el.attr('data-product-src');
			let color = el.attr('data-product-color');
			let styleClass = el.attr('data-product-class');
			let quantity = 1;
	
			return { id, name, price, quantity, src, color, styleClass };
		}
	
		const addCartItem = (data) => {
			const { id } = data;
			cart[id] = data;
			updateCart();
		}

		const addQuickItem = (data) => {
			quick = data;
			localStorage.setItem('quick', JSON.stringify(quick));
		}		
	
		const deleteCartItem = (id) => {
	
			let cartItemE = cartE.find(`.js-cart-item[data-product-id="${id}"]`);
			if (!cartItemE) { return; }
	
			cartItemE.remove();
			delete cart[id];
	
			updateCart();
		}
	
		// Сохранение корзины в локальное хранилище
		const saveCart = () => {
			localStorage.setItem('cart', JSON.stringify(cart));
		}
	
		// Обновление общей суммы
		const updateCartTotalPrice = () => {
			
			let totalPrice = Object.keys(cart).reduce((acc, id) => {
				const { price, quantity } = cart[id];
				return acc + price * quantity;
			}, 0);
	
			if (cartTotalPriceE) {
				cartTotalPriceE.text(totalPrice);
			}
	
		}
	
		// Обновление корзины
		const updateCart = () => {
			updateCartTotalPrice();
			saveCart();
		}
	
		// Рендеринг одного товара для страницы корзины
		const renderCartItem = ({ id, name, price, quantity, src, color, styleClass }) => {
	
			const cartItemE = $(document.createElement('li'));
			cartItemE.addClass('cart__item js-cart-item');
			cartItemE.attr('data-product-id', id);
	
			const cartItemTemplate = `
					<article class="cart__body ${styleClass}">
						<div class="cart__image-wrapper">
							<img class="cart__image" src="${WPJS.siteUrl + src}" alt="#">
						</div>
	
						<div class="cart__characteristics-wrapper">
							<div class="cart__characteristics">
								<div class="cart__color-wrapper">
									<span class="cart__color"></span>
									<span class="cart__color-img"></span>
								</div>
	
								<div class="cart__text-wrapper">
									<p class="cart__color-text">${color}</p>
									<p class="cart__name">${name}</p>
								</div>
							</div>
	
						</div>
	
						<div class="cart__price-column">
							<button class="cart__delited js-cart-delete"><span>Удалить</span></button>
							<p class="cart__price">${price} ₽</p>
						</div>
	
						<input type="hidden" name="${id}-name" value="${name}">
						<input type="hidden" name="${id}-price" value="${price}">
						<input type="hidden" name="${id}-quantity" value="${quantity}">
	
					</article>
			`;
	
			cartItemE.html(cartItemTemplate);
			cartE.append(cartItemE);
		}
	
		// Вывод всех товаров из корзины
		const renderCart = () => {
			for (let key in cart) {
				renderCartItem(cart[key]);
			}
		}

		// Вывод товара для страницы Быстрой покупки
		const renderQuick = () => {

			if ($.isEmptyObject(quick)) {
				return;
			}

			let { id, name, price, src, color, styleClass } = quick;
			const quickItemE = $(document.createElement('li'));
			quickItemE.addClass('cart__item');
			quickItemE.attr('data-product-id', id);

			const quickItemTemplate = `
				<article class="cart__body ${styleClass}">
					<div class="cart__image-wrapper">
						<img class="cart__image" src="${WPJS.siteUrl + src}" alt="#">
					</div>

					<div class="cart__characteristics-wrapper">
						<div class="cart__characteristics">
							<div class="cart__color-wrapper">
								<span class="cart__color"></span>
								<span class="cart__color-img"></span>
							</div>

							<div class="cart__text-wrapper">
								<p class="cart__color-text">${color}</p>
								<p class="cart__name">${name}</p>
							</div>
						</div>
					</div>

					<div class="cart__price-column">
						<p class="cart__price">${price} ₽</p>
					</div>
				</article>
			`;
			quickItemE.html(quickItemTemplate);
			quickE.append(quickItemE);
			quickTotalPriceE.text(price);
		}
	
		// Инициализация корзины на странице Корзины
		const cartInit = () => {
	
			renderCart();
			updateCart();
	
			$(document).on('click', '.js-cart-delete', (ev) => {
				ev.preventDefault();
				el = $(ev.target);
				
				const cartItemE = el.parents('.js-cart-item');
				let id = cartItemE.attr('data-product-id');
	
				deleteCartItem(id);
			})
		}
		// Условие, что мы на странице корзины
		if ($('main').hasClass('js-page-cart')) {
			cartInit();
		}


		// Инициализация страницы быстрой покупки
		const quickInit = () => {
			renderQuick();
		}

		if ($('main').hasClass('js-page-quick')) {
			quickInit();
		}


	
	
		// Нажатие кнопки "Купить" в карточке товара
		$(document).on('click', '.js-buy', (ev) => {
			// el = $(ev.target);
			el = $('.slider-product__item.slick-current');
			if (!el) { return false; }
			data = getProductData(el);
			addCartItem(data);
		})
	
	
		// Клик на "Быструю покупку" в каталоге
		$(document).on('click', '.js-quick-buy', (ev) => {
			let el = $(ev.target).parents('.catalog__item');
			if (!el) { return false};
			let data = getProductData(el);
			addQuickItem(data);
		})

	
		// Клик на товар в каталоге
		$('.catalog__link').on('click', function (ev) {
			ev.preventDefault();
			let productE = $(this).parents('.catalog__item');
			if (productE) {
				let id = productE.attr('data-product-id');
				let temp = $(this).attr('href');
				temp = temp + '/?id=' + id;
				document.location.href = temp;
			}
		})
	
		/*
		 *	Корзина КОНЕЦ
		 */ 

		// Быстрая покупка
		$('.js-button-quick-send').on('click', (ev) => {
			ev.preventDefault();

			let productData = serializeQuick();
			let clientData = serializeClient($('.cart__form'));

			if ( '' === productData || '' === clientData ) {
				return;
			}

			data = {
				client: clientData,
				product: productData,
			}

			$.post(
				WPJS.ajaxUrl + '?action=send_email_quick',
				data,
				function (responce) {
					if ('success' == responce) {
						showSendQuickMessage($('.cart__column--order'));
						localStorage.removeItem('quick');
						quick = {};
					} else {
						console.log(responce);
					}
				}
			)
		})
		
		/*
		 *	Оформление Заказа НАЧАЛО
		 */

		//Отправка заказа из Корзины
		$('.js-button-order-send').on('click', (ev) => {
			ev.preventDefault();

			let productsData = serializeProducts();
			let clientData = serializeClient($('.cart__form'));

			if ( '' === productsData || '' === clientData ) {
				return;
			}

			let data = {
				products: productsData,
				client: clientData,
			}

			$.post(
				WPJS.ajaxUrl + '?action=send_email',
				data,
				function (response) {
					if ('success' == response) {
						showSendOrderMessage($('.cart__column--order'));
						localStorage.removeItem('cart');
						cart = {};
					} else {
						console.log(response);
					}
				}
			);

		})

		// Сообщение об отправке Быстрого заказа
		let showSendQuickMessage = (container) => {
			if (!container) return;

			const clientName = $('.form-cart__input[name=name]').val();

			const messageE = $(document.createElement('div'));
			messageE.addClass('cart__thanks-wrapper');
			const msg = `
				<p class="cart__thanks-text cart__thanks-text--size"><strong><span class="cart__br">${clientName}, спасибо</span> за ваш заказ!</strong></p>

				<p class="cart__thanks-text">На ваш телефонный номер придёт СМС с подтверждением, и в течение 15 минут наш менеджер свяжется с вами:</p>
				<p class="cart__thanks-text cart__thanks-text--size"><strong>999 12 231</strong></p>
				<a class="cart__button button button-primary" href="/">Перейти на главную</a>
			`;
			messageE.html(msg);
			$('.cart__form').remove();
			container.append(messageE);
		}


		// Сообщение об отправке Заказа
		let showSendOrderMessage = (container) => {
			if (!container) return;

			const clientName = $('.form-cart__input[name=name]').val();

			const messageE = $(document.createElement('div'));
			const msg = `
				<div class="cart__thanks-wrapper">
					<p class="cart__thanks-text cart__thanks-text--size"><strong><span class="cart__br">${clientName}, спасибо</span> за ваш заказ!</strong></p>

					<p class="cart__thanks-text">На ваш телефонный номер придёт СМС с подтверждением, и в течение 15 минут наш менеджер свяжется с вами:</p>
					<p class="cart__thanks-text cart__thanks-text--size"><strong>121 12 231</strong></p>
					<a class="cart__button button button-primary" href="/">Перейти на главную</a>
				</div>
			`;
			messageE.html(msg);
			$('.cart__form').remove();
			container.append(messageE);
		}


		// Сериализация товара для быстрой покупки
		let serializeQuick = function () {
			separator = '&&&';
			let { id, name, price } = quick;
			str = id + separator + name + separator + price;
			return str;
		}

		// Сериализация формы
		let serializeProducts = function () {
			let str = '';
			let separator = '';
			for (let key in cart) {
				let { id, name, price, quantity } = cart[key];
				str += separator + id + ',' + name + ',' + quantity + ',' + price;
				separator = '&&&';
			}
			return str;
		};

		// Сериализация данных клиента
		let serializeClient = function (form) {
			let name = form.find('.form-cart__input[name=name]').val();
			let emaill = form.find('.form-cart__input[name=email]').val();
			let phone = form.find('.form-cart__input[name=phone]').val();

			let str = 'Имя: ' + name + '&&&' + 'Телефон: ' + phone + '&&&Email: ' + emaill;

			return str;
		}
		/*
		 *	Оформление Заказа КОНЕЦ
		 */ 
		
	}());
	
	
	
	/*
	*	ДИНАМИЧЕСКИЙ АДАПТИВ
	*/
	
	// Dynamic Adapt v.1
	// HTML data-da="where(uniq class name),position(digi),when(breakpoint)"
	// e.x. data-da="item,2,992"
	// Andrikanych Yevhen 2020
	// https://www.youtube.com/c/freelancerlifestyle
	
	"use strict";
	
	(function () {
	
		let originalPositions = [];
		let daElements = document.querySelectorAll('[data-da]');
		let daElementsArray = [];
		let daMatchMedia = [];
	
		//Заполняем массивы
		if (daElements.length > 0) {
			let number = 0;
			for (let index = 0; index < daElements.length; index++) {
				const daElement = daElements[index];
				const daMove = daElement.getAttribute('data-da');
				if (daMove != '') {
					const daArray = daMove.split(',');
					const daPlace = daArray[1] ? daArray[1].trim() : 'last';
					const daBreakpoint = daArray[2] ? daArray[2].trim() : '767';
					const daType = daArray[3] === 'min' ? daArray[3].trim() : 'max';
					const daDestination = document.querySelector('.' + daArray[0].trim())
	
					if (daArray.length > 0 && daDestination) {
						daElement.setAttribute('data-da-index', number);
						//Заполняем массив первоначальных позиций
						originalPositions[number] = {
							"parent": daElement.parentNode,
							"index": indexInParent(daElement)
						};
						//Заполняем массив элементов 
						daElementsArray[number] = {
							"element": daElement,
							"destination": document.querySelector('.' + daArray[0].trim()),
							"place": daPlace,
							"breakpoint": daBreakpoint,
							"type": daType
						}
						number++;
					}
				}
			}
			dynamicAdaptSort(daElementsArray);
	
			//Создаем события в точке брейкпоинта
			for (let index = 0; index < daElementsArray.length; index++) {
				const el = daElementsArray[index];
				const daBreakpoint = el.breakpoint;
				const daType = el.type;
	
				daMatchMedia.push(window.matchMedia("(" + daType + "-width: " + daBreakpoint + "px)"));
				daMatchMedia[index].addListener(dynamicAdapt);
			}
		}
	
		//Основная функция
		function dynamicAdapt(e) {
			for (let index = 0; index < daElementsArray.length; index++) {
				const el = daElementsArray[index];
				const daElement = el.element;
				const daDestination = el.destination;
				const daPlace = el.place;
				const daBreakpoint = el.breakpoint;
				const daClassname = "_dynamic_adapt_" + daBreakpoint;
	
				if (daMatchMedia[index].matches) {
					//Перебрасываем элементы
					if (!daElement.classList.contains(daClassname)) {
						let actualIndex = indexOfElements(daDestination)[daPlace];
						if (daPlace === 'first') {
							actualIndex = indexOfElements(daDestination)[0];
						} else if (daPlace === 'last') {
							actualIndex = indexOfElements(daDestination)[indexOfElements(daDestination).length];
						}
						daDestination.insertBefore(daElement, daDestination.children[actualIndex]);
						daElement.classList.add(daClassname);
					}
				} else {
					//Возвращаем на место
					if (daElement.classList.contains(daClassname)) {
						dynamicAdaptBack(daElement);
						daElement.classList.remove(daClassname);
					}
				}
			}
			customAdapt();
		}
	
		//Вызов основной функции
		dynamicAdapt();
	
		//Функция возврата на место
		function dynamicAdaptBack(el) {
			const daIndex = el.getAttribute('data-da-index');
			const originalPlace = originalPositions[daIndex];
			const parentPlace = originalPlace['parent'];
			const indexPlace = originalPlace['index'];
			const actualIndex = indexOfElements(parentPlace, true)[indexPlace];
			parentPlace.insertBefore(el, parentPlace.children[actualIndex]);
		}
		//Функция получения индекса внутри родителя
		function indexInParent(el) {
			var children = Array.prototype.slice.call(el.parentNode.children);
			return children.indexOf(el);
		}
		//Функция получения массива индексов элементов внутри родителя 
		function indexOfElements(parent, back) {
			const children = parent.children;
			const childrenArray = [];
			for (let i = 0; i < children.length; i++) {
				const childrenElement = children[i];
				if (back) {
					childrenArray.push(i);
				} else {
					//Исключая перенесенный элемент
					if (childrenElement.getAttribute('data-da') == null) {
						childrenArray.push(i);
					}
				}
			}
			return childrenArray;
		}
		//Сортировка объекта
		function dynamicAdaptSort(arr) {
			arr.sort(function (a, b) {
				if (a.breakpoint > b.breakpoint) { return -1 } else { return 1 }
			});
			arr.sort(function (a, b) {
				if (a.place > b.place) { return 1 } else { return -1 }
			});
		}
		//Дополнительные сценарии адаптации
		function customAdapt() {
			//const viewport_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		}



	

		/*
		 *	Выбор языка
		 */

		$(document). on('click', '.js-select-language', function (ev) {

			let lang = $(this).attr('data-lang');
			if (lang) {

				let date = new Date(Date.now() + 1209600e3);
				date = date.toUTCString();
				
				switch (lang) {
					case 'it':
						document.cookie = "language=it; path=/; expires=" + date;
						break;
					case 'ru':
						document.cookie = "language=ru; path=/; expires=" + date;
						break;
					case 'en':
						document.cookie = "language=en; path=/; expires=" + date;
						break;
				}

			}

		});



	}());

});





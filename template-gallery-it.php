<?php get_header(); ?>

<main>
      <section class="gallery">
         <div class="gallery__container">
            <h1 class="gallery__title">Galleria</h1>

            <div class="gallery__flex">

               <div class="gallery__row">

                  <div class="gallery__column">
                     <div class="gallery__wrapper-img gallery__wrapper-img--one">
                        <img class="gallery__image gallery__image--medium" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-1.jpg"?>" alt="#">

                        <img class="gallery__image gallery__image--small right"
                           data-da="gallery__wrapper-img--two, 1, 767" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-2.jpg"?>" alt="#">
                     </div>

                     <div class="gallery__wrapper-img gallery__wrapper-img--two">

                        <img class="gallery__image gallery__image--small"
                           data-da="gallery__wrapper-img--one, 1, 767" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-3.jpg"?>" alt="#">

                        <img class="gallery__image gallery__image--medium" data-da="gallery__wrapper-img--one, 3, 767"
                           src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-4.jpg"?>" alt="#">
                     </div>
                  </div>

                  <div class="gallery__wrapper-img">
                     <img class="gallery__image gallery__image--big" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-5.jpg"?>" alt="#">
                  </div>
               </div>

               <div class="gallery__row gallery__row--reverse">
                  <div class="gallery__column gallery__column--two">

                     <div class="gallery__wrapper-img">
                        <img class="gallery__image gallery__image--medium" data-da="gallery__wrapper-img--three, 0, 767"
                           src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-6.jpg"?>" alt="#">

                        <img class="gallery__image gallery__image--small left" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-7.jpg"?>" alt="#">
                     </div>

                     <div class="gallery__wrapper-img gallery__wrapper-img--three">
                        <img class="gallery__image gallery__image--small right" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-8.jpg"?>" alt="#">
                        <img class="gallery__image gallery__image--medium" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-9.jpg"?>" alt="#">
                     </div>
						</div>

						<div class="gallery__wrapper-img gallery__wrapper-img--none">
							<img class="gallery__image gallery__image--big" src="<?= get_template_directory_uri() . "/assets/img/gallery/gallery-5.jpg"?>" alt="#">
						</div>

					</div>
					
            </div>

         </div>
      </section>
   </main>



<?php get_footer(); ?>